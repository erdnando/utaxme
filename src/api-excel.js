import axios from 'axios';

export default() => {
    return axios.create({
        baseURL: `https://utaxon.com/billerTicket-mobile/`,
        withCredentials: false,
        responseType: 'blob',
        headers: {
            'Accept': '*/*'
        }
    });
}