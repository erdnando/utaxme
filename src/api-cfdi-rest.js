import axios from 'axios';

export default() => {
    return axios.create({
        baseURL: `https://utaxon.com/billerTicket-mobile/`,
        withCredentials: false,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    });
}