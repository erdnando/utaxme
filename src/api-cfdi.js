import axios from 'axios';

export default() => {
    return axios.create({
        baseURL: `https://utaxon.com/billerTicket-mobile/`,
        withCredentials: false,
        headers: {
            'Accept': '*/*',
            'Content-Type': 'text/plain'
        }
    });
}